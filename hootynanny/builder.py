from audio2numpy import open_audio
import lowess
import pandas as pd
import pathlib
import numpy as np

from matplotlib import pyplot as plt
from matplotlib.pyplot import plot
from matplotlib.image import imread
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

from moviepy.editor import VideoClip, AudioFileClip
from moviepy.video.io.bindings import mplfig_to_npimage
import scipy.ndimage as ndimage

from sklearn.decomposition import PCA

from itertools import groupby
from operator import itemgetter

import colorsys


def load_audio(music_path):
    signal, sampling_rate = open_audio(music_path)
    print(f"Sampling rate: {sampling_rate}")
    print(f"Channels: {signal.shape[1]}")
    print(f"Length: {round(signal.shape[0] / sampling_rate, 3)}(sec)")
    return signal, sampling_rate


def calc_displacement(signal):
    return np.sum(np.abs(signal[1:] - signal[:-1]), axis=1)


def freqrep(channel, repdist, repsize):
    rep = []
    nreps = (len(channel) - repsize) // repdist
    for i in range(nreps):
        rep.append(np.real(np.fft.rfft(channel[(i * repdist):(i * repdist + repsize)])))
    return np.array(rep)


def lowessinterp(x, y, bw=0.01, p=3):
    ynew = np.array(lowess.lowess(pd.Series(x), pd.Series(y), bandwidth=bw, polynomialDegree=p))
    return [x, ynew]


def freqsmooth(fr, freq_cutoff, freq_smooth_bw, freq_smooth_lagadjust):
    x = np.arange(fr.shape[0])
    y = np.average(np.tile(np.arange(freq_cutoff), fr.shape[0]).reshape((fr.shape[0], freq_cutoff)),
                   axis=1,
                   weights=np.maximum(np.abs(fr[:, :freq_cutoff]), 1e-6))

    xs, ys = lowessinterp(x, y, freq_smooth_bw)
    lag = int(len(x) * freq_smooth_bw / freq_smooth_lagadjust)
    return np.pad(ys[lag:], (0, lag), constant_values=np.median(ys))


def index_groups(idexes):
    groups = []
    for k, g in groupby(enumerate(idexes), lambda x: x[0] - x[1]):
        group = (map(itemgetter(1), g))
        group = list(map(int, group))
        groups.append(group)
    return groups


def peak_detector(s, q=0.999):
    thresh = np.quantile(s, q=q)
    high = np.where(s > thresh)[0]
    groups = index_groups(high)
    peaks = []
    for g in groups:
        sg = s[g]
        maxi = np.argmax(sg)
        peaks.append((g[maxi], sg[maxi]))
    return peaks


def filter_peaks(peaks, min_dist):
    peaks = sorted(peaks, key=lambda x: x[1], reverse=True)

    keepers = [peaks[0]]
    vals = [peaks[0][0]]
    for p in peaks[1:]:
        if np.min(np.abs(np.array(vals) - p[0])) >= min_dist:
            keepers.append(p)
            vals.append(p[0])

    return sorted(keepers, key=lambda x: x[0])


def ustandardize(x):
    mn = np.mean(x)
    return (x - mn) / (max(np.max(x) - mn, mn - np.min(x))) / 2 + 0.5


def build_laserdata(pc):
    pcus = [ustandardize(x) for x in pc.components_[:3]]
    laserxy = np.random.random((2, len(pcus[0]))) - 0.5
    laserxy *= 1000 / np.linalg.norm(laserxy, axis=0)
    lasercolor = [colorsys.rgb_to_hsv(pcus[0][li], pcus[1][li], pcus[2][li]) for li in range(laserxy.shape[1])]
    lasercolor = [colorsys.hsv_to_rgb(lc[0], lc[0], 1) for lc in lasercolor]
    return [laserxy, lasercolor]


def wavefit(x):
    mn = np.mean(x)
    mfft = np.fft.fft(x - mn)
    mfft[0] = 0  # Don't allow no-wave
    imax = np.argmax(np.absolute(mfft))
    mask = np.zeros_like(mfft)
    mask[[imax]] = 1
    mfft *= mask
    return np.real(np.fft.ifft(mfft)) + mn


def wavesmooth(x, repdist, repsize):
    nreps = (len(x) - repsize) // repdist
    total = np.zeros_like(x)
    count = np.zeros_like(x)
    for i in range(nreps):
        a = (i * repdist)
        b = (i * repdist + repsize)
        wf = wavefit(x[a:b])
        total[a:b] += wf
        count[a:b] += 1

    return total / count


def dampstart(x):
    mn = np.mean(x[:25])
    x[:25] = mn + (x[:25] - mn) * np.linspace(0, 1, 25)
    return


def render(duration, room_light, music_path, body_samples, peaktimes, beat_lead, beat_lag, laserdata, sfreq_normwave,
           signal, sampling_rate, fps, out_path):

    laserxy, lasercolor = laserdata

    # Load assets
    asset_path = str(pathlib.Path(__file__).parent.resolve()) + '/assets'
    img_bkg = imread(asset_path + '/OwlHouse.jpg')
    oi_bkg = OffsetImage(img_bkg / 255 * room_light, zoom=0.4)
    img_house = imread(asset_path + '/HootyHouse.png')
    oi_house = OffsetImage(img_house, zoom=0.2)
    img_head = np.flip(imread(asset_path + '/HootyHeadphones.png'), axis=1)
    audio = AudioFileClip(music_path)

    # Establish figure
    x = np.linspace(-1, 1, body_samples)
    fig, ax = plt.subplots()
    fig.set_figheight(4.5)
    fig.set_figwidth(8)
    fig.set_dpi(400)
    fig.patch.set_facecolor('black')

    # Frame function
    def make_frame(t):
        # Clear the figure
        ax.clear()
        ax.axis('off')

        # Onbeat status
        onbeat = np.any(np.logical_and((t - peaktimes) <= beat_lag / 1000, (t - peaktimes) >= -beat_lead / 1000))

        # Background
        ab = AnnotationBbox(oi_bkg, (0.5, 0.5), frameon=False, zorder=0)
        ax.add_artist(ab)

        # House
        ab = AnnotationBbox(oi_house, (0.51, 0.21), frameon=False, zorder=0.01)
        ax.add_artist(ab)

        # Lasers
        laseri = range(int(10 * t - 10), min(int(10 * t), laserxy.shape[1]))
        for li in laseri:
            plot([0.5, laserxy[0, li]], [0.2, laserxy[1, li]],
                 color=lasercolor[li] + (0.9, ),
                 solid_capstyle='round',
                 lw=3)
            plot([0.5, laserxy[0, li] + 10], [0.2, laserxy[1, li] + 10],
                 color=lasercolor[li] + (0.5, ),
                 solid_capstyle='round',
                 lw=5)
            plot([0.5, laserxy[0, li] - 10], [0.2, laserxy[1, li] - 10],
                 color=lasercolor[li] + (0.5, ),
                 solid_capstyle='round',
                 lw=5)

        # Body
        f = sfreq_normwave[max(int(10 * t - body_samples), 0):min(int(10 * t), len(sfreq_normwave))]
        f = np.pad(f, (body_samples - len(f), 0), constant_values=sfreq_normwave[0])
        if np.isnan(f[-1]):
            f[-1] = f[-2]

        f = np.clip(f, 0.0, 1.0)
        ax.plot(x, f - 0.03, lw=42, color='black', solid_capstyle='round')
        ax.plot(x, f, lw=42, color='black', solid_capstyle='round')
        ax.plot(x, f - 0.03, lw=40, color='chocolate', solid_capstyle='round')
        ax.plot(x, f, lw=40, color='peru', solid_capstyle='round')

        # Head
        tilt = 2 * np.pi * np.arctan((f[-1] - f[-2]) / (x[-1] - x[-2]))
        if np.isnan(tilt):
            tilt = 0
        if onbeat:
            tilt -= 30
        oi_head = OffsetImage(np.clip(ndimage.rotate(img_head, tilt, reshape=True), 0, 1), zoom=0.15)
        ab = AnnotationBbox(oi_head, (x[-1], f[-1]), frameon=False)
        ax.add_artist(ab)

        # Render
        ax.set_xlim(-.2, 1.2)
        ax.set_ylim(-0.05, 1.05)
        fig.tight_layout(pad=0)
        return mplfig_to_npimage(fig)

    animation = VideoClip(make_frame, duration=len(signal) / sampling_rate)
    animation.audio = audio
    if duration is not None:
        animation = animation.subclip(0, duration)  # For debugging purposes
    animation.write_videofile(out_path, fps=fps)


def build_hootynanny(music_path,
                     out_path,
                     duration=None,
                     fps=20,
                     freq_cutoff=200,
                     freq_smooth_bw=0.01,
                     freq_smooth_lagadjust=3,
                     freq_wavesmooth_width=25,
                     body_channel=0,
                     body_samples=100,
                     beat_thresh=0.99,
                     beat_lead=200,
                     beat_lag=100,
                     room_light=0.25):

    # Frequency at which the dance is simulated (hertz)
    SIMULATION_FREQ = 10  # 10/s, 100ms

    # Load the audio
    signal, sampling_rate = load_audio(music_path)
    nchannels = signal.shape[1]

    # Compute frequency representations
    print("Computing channel frequency representations")
    channel_fr = [
        freqrep(signal[:, ci], int(sampling_rate / SIMULATION_FREQ), int(sampling_rate / SIMULATION_FREQ))
        for ci in range(nchannels)
    ]

    # Smooth frequency
    sfreq = freqsmooth(channel_fr[body_channel], freq_cutoff, freq_smooth_bw, freq_smooth_lagadjust)
    sfreq_norm = np.clip((sfreq - np.median(sfreq)) / np.median(np.abs(sfreq - np.median(sfreq))) / 1.4826 / 4 + 0.5,
                         0.0, 1)
    sfreq_normwave = wavesmooth(sfreq_norm, 1, freq_wavesmooth_width)
    # Dampen the first part of the wave for a smoother start
    dampstart(sfreq_normwave)

    # Detect peaks
    print("Detecting peaks")
    displacement = calc_displacement(signal)
    peaks = peak_detector(displacement, q=beat_thresh)
    peaks = filter_peaks(peaks, sampling_rate / 3)
    peaktimes = np.array([p[0] for p in peaks]) / sampling_rate

    # Compute laser data
    print("Computing laser data")
    pc = PCA(n_components=3)
    pc.fit(np.concatenate(channel_fr, axis=1).T)
    laserdata = build_laserdata(pc)

    # Render video
    print("Rendering video")
    render(duration, room_light, music_path, body_samples, peaktimes, beat_lead, beat_lag, laserdata, sfreq_normwave,
           signal, sampling_rate, fps, out_path)

    return
