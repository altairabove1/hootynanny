#!/usr/bin/env python3
import argparse
import os
from hootynanny.builder import build_hootynanny


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('musicpath', help='Music file path')
    parser.add_argument('--outpath', '-o', help='Output .mp4 file path (default is based on musicpath)')
    parser.add_argument('--duration', type=float, default=None, help='Duration of movie to render (sec)')
    parser.add_argument('--fps', type=float, default=20, help='Frame rate of the video')
    parser.add_argument('--freq_cutoff', type=int, default=200, help='Frequency cutoff for wave analysis')
    parser.add_argument('--freq_smooth_bw', type=float, default=0.01, help='Wave smoothing bandwidth')
    parser.add_argument('--freq_smooth_lagadjust',
                        type=float,
                        default=3,
                        help='1/x fraction of bandwidth to adjust for apparent lag from smoothing')
    parser.add_argument('--freq_wavesmooth_width', type=int, default=25, help='Window width of wave smoothing')
    parser.add_argument('--body_channel', type=int, default=0, help='Audio channel for body movement')
    parser.add_argument('--body_samples', type=int, default=200, help='Number of body samples')
    parser.add_argument('--beat_thresh', type=float, default=0.99, help='Percentile cutoff for peak detection')
    parser.add_argument('--beat_lead', type=int, default=200, help='Head beat lead time (ms)')
    parser.add_argument('--beat_lag', type=int, default=100, help='Head beat lag time (ms)')
    parser.add_argument('--room_light', type=float, default=0.25, help='Room light intensity (0-1)')

    args = parser.parse_args()
    if args.outpath is None:
        args.outpath = os.path.splitext(args.musicpath)[0] + '.hoot.mp4'
    build_hootynanny(args.musicpath, args.outpath,
                     **{k: v
                        for k, v in vars(args).items() if k not in {'musicpath', 'outpath'}})


if __name__ == "__main__":
    main()
