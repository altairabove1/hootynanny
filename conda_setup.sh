#!/bin/bash

conda deactivate
conda env remove -y -n hootynanny
conda clean -y --all
conda update -y -n base conda
conda install -y conda-build

conda create -y -n hootynanny python=3.7
conda activate hootynanny
conda update pip

conda install -y -c conda-forge moviepy pyqt tqdm
conda install -y -c anaconda flake8 git jupyter numpy pandas scikit-learn scipy tk yaml yapf

conda develop .

~/miniconda3/envs/hootynanny/bin/pip3 install --upgrade matplotlib pillow audio2numpy lowess

conda update -y --all
