# Hootynanny

## A tool to render Hooty grooving to any music.

## [Video demo](https://youtu.be/gM_X1lPFbCU)

## Getting hootynanny
`git clone git@gitlab.com:altairabove1/hootynanny.git`

## Installation
Hootynanny currently requires a specific conda environment to run. First install miniconda in your home folder, then run:

`cd hootynanny`

`source conda_setup.sh`

## Example usage

Default parameters:

`./hootynanny/hoot.py mymusic.mp3`

Custom video path and parameters:

`./hootynanny/hoot.py mymusic.mp3 -o hootvideo.mp4 --beat_thresh 0.98 --fps 30`


## Installing with pip (known bug)

Hootynanny is set up to work with pip install, but there is a rendering bug/incompatability with the associated libraries.
Ideally, this would be the installation process:

`cd hootynanny`

`pip install .`

The program can then be called simply with:

`hoot mymusic.mp3`.
